#ifndef _MEM_H_
#define _MEM_H_

#include <stdio.h>

#define HEAP_START ((void *)0x04040000)

/**
 * Выделяет `query` байт. Возвращает указатель на выделенный блок
 * @param query минимальный размер блока
 * @returns указатель на начало блока в случае успеха, NULL если выделить
 *память не удалось
 **/
void *_malloc(size_t query);
/**
 * Освобождает блок, начинающийся по адресу `mem`.
 **/
void _free(void *mem);
/**
 * Инициализирует кучу изначального размера в `initial_size` байт.
 * Возвращает указатель на начало кучи.
 */
void *heap_init(size_t initial_size);
void heap_term(void);

#define DEBUG_FIRST_BYTES 4

void debug_struct_info(FILE *f, void const *address);
void debug_heap(FILE *f, void const *ptr);

#endif
