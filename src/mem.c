
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif

#include <assert.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) {
  return block->capacity.bytes >= query;
}

static size_t pages_count(size_t mem) {
  size_t pagesize = sysconf(_SC_PAGESIZE);
  return (mem + pagesize - 1) / (pagesize);
}

static size_t round_pages(size_t mem) {
  size_t pagesize = sysconf(_SC_PAGESIZE);
  return pagesize * pages_count(mem);
}

static void block_init(void *restrict addr, block_size block_sz,
                       void *restrict next) {
  *((struct block_header *)addr) = (struct block_header){
      .next = next, .capacity = capacity_from_size(block_sz), .is_free = true};
}

static size_t region_actual_size(size_t query) {
  return size_max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

/**
 * Запрашивает `length` байт памяти у ядра и маппит их ориентируясь на аргумент
 * `addr`.
 * @param `addr` желаемый адрес начала
 * @param `length` длина участка памяти
 * @param `additional_flags` дополнительные флаги для системного выозова mmap
 * @returns Адрес начала замапленой памяти
 */
static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE,
              MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(const void *addr, size_t query) { /*  ??? */
  const size_t region_size =
      region_actual_size(query + offsetof(struct block_header, contents));
  void *actual_addr =
      map_pages(addr, region_size, MAP_FIXED | MAP_FIXED_NOREPLACE);
  bool extends = true;
  if (actual_addr == MAP_FAILED) {
    actual_addr = map_pages(addr, region_size, 0);
    if (actual_addr == MAP_FAILED) {
      return REGION_INVALID;
    }
    extends = false;
  }
  struct region region = {
      .addr = actual_addr,
      .size = region_size,
      .extends = extends,
  };
  block_init(region.addr, (block_size){region_size}, NULL);
  return region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )---
 */

static bool block_splittable(struct block_header *restrict block,
                             size_t query) {
  return block->is_free &&
         query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <=
             block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
  if (block_splittable(block, query)) {
    void *first_block_addr = (void *)block;

    block_size first_block_size = size_from_capacity((block_capacity){query});
    void *second_block_addr = first_block_addr + first_block_size.bytes;
    block_size second_block_size = (block_size){block->capacity.bytes - query};
    void *next_block = block->next;
    block_init(first_block_addr, first_block_size, second_block_addr);
    block_init(second_block_addr, second_block_size, next_block);
    return true;
  }
  return false;
}

/*  --- Слияние соседних свободных блоков --- */

/**
 * Возвращает указатель на участок памяти сразу за данным блоком
 */
static void *block_after(struct block_header const *block) {
  return (void *)(block->contents + block->capacity.bytes);
}
static bool blocks_continuous(struct block_header const *fst,
                              struct block_header const *snd) {
  return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst,
                      struct block_header const *restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) { /*  ??? */
  if (block->next == NULL) {
    return false;
  }
  bool is_mergeable_with_next = mergeable(block, block->next);
  if (is_mergeable_with_next) {
    block->capacity =
        (block_capacity){block->capacity.bytes + block->next->capacity.bytes +
                         offsetof(struct block_header, contents)};
    block->next = block->next->next;
    return true;
  }
  return false;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header *block;
};

static struct block_search_result
find_good_or_last(struct block_header *restrict block, size_t sz) {
  while (block->next != NULL) {
    while (try_merge_with_next(block))
      ;
    if (block->is_free && block_is_big_enough(sz, block)) {
      return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
    }
    block = block->next;
  }
  if (block->is_free && block_is_big_enough(sz, block)) {
    return (struct block_search_result){BSR_FOUND_GOOD_BLOCK, block};
  }
  return (struct block_search_result){BSR_REACHED_END_NOT_FOUND, block};
}

/**
 * Возвращает наименьшее число, выравненное по 8, больше данного
 **/
static size_t greater_than_aligned(size_t query) {
  const size_t alignment = 8;
  return (query + (alignment - query % alignment) % alignment);
}

/**
 * Пытается выделить блок вместимостью как минимум в `query` байт.
 * Не будет пытаться добавить в кучу новые регионыж.
 * @params `query` минимальная вместимость блока
 * @params `block` блок, после которого нужно выделить новый
 * @returns Результат поиска блока
 **/
static struct block_search_result
try_memalloc_existing(size_t query, struct block_header *block) {
  const size_t size_to_query_unaligned =
      query > BLOCK_MIN_CAPACITY ? query : BLOCK_MIN_CAPACITY;
  const size_t size_to_query = greater_than_aligned(size_to_query_unaligned);
  struct block_search_result first_good =
      find_good_or_last(block, size_to_query);
  if (first_good.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(first_good.block, size_to_query);
    first_good.block->is_free = false;
    return first_good;
  }
  return first_good;
}

/**
 * Пытается добавить новый регион вместимостью как минимум в `query` байт.
 * Добавляет новый регион следом за элементом `last` и инициализирует его
 * блоком.
 * @returns Указатель на заголовок блока в новом регионе в случае успеха, `NULL`
 * в случае неудачи.
 */
static struct block_header *grow_heap(struct block_header *restrict last,
                                      size_t query) {
  assert(last->next == NULL);
  void *addr_to_map = block_after(last);
  struct region new_region = alloc_region(addr_to_map, query);
  if (new_region.addr == NULL) {
    return NULL;
  }
  last->next = new_region.addr;
  if (try_merge_with_next(last)) {
    return last;
  }
  return new_region.addr;
}

/**
 * Пытается выделить блок вместимостью в как минимум `query` байт
 * @param `heap_start` указатель на начало участка кучи
 * @param `query` необходимое количество байт
 * @returns Указатель на заголовок блока в случае успеха, `NULL` в случае
 *неудачи
 **/
static struct block_header *memalloc(size_t query,
                                     struct block_header *heap_start) {
  struct block_search_result res = try_memalloc_existing(query, heap_start);
  if (res.type == BSR_FOUND_GOOD_BLOCK) {
    return res.block;
  }
  if (res.type == BSR_REACHED_END_NOT_FOUND) {
    struct block_header *last = res.block;
    grow_heap(last, query);
    res = try_memalloc_existing(query, heap_start);
    if (res.type == BSR_FOUND_GOOD_BLOCK) {
      return res.block;
    }
  }
  return NULL;
}

/**
 * Пытается выделить блок размером как минимум в `query` байт.
 * @returns адрес содержимого блока в случае успеха, `NULL` в случае неудачи
 **/
void *_malloc(size_t query) {
  struct block_header *const addr =
      memalloc(query, (struct block_header *)HEAP_START);
  if (addr) {
    return addr->contents;
  } else {
    return NULL;
  }
}

/**
 * Получает адрес заголовка блока по адресу его содержимого.
 **/
static struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) -
                                 offsetof(struct block_header, contents));
}

/**
 * Помечает блок памяти, начинающийся по адресу `mem` как свободный
 **/
void _free(void *mem) {
  if (!mem)
    return;
  struct block_header *header = block_get_header(mem);
  header->is_free = true;
  while (try_merge_with_next(header))
    ;
}

/**
 * Освобождает всю память, выделенную под кучу
 **/
void heap_term() {
  struct block_header *start = (struct block_header *)HEAP_START;
  struct block_header *current = start;
  size_t current_region_size = 0;
  while (current != NULL) {
    current_region_size += size_from_capacity(current->capacity).bytes;
    if (current->next != NULL && blocks_continuous(current, current->next)) {
      current = current->next;
      continue;
    }
    current = current->next;
    munmap(start, round_pages(current_region_size));
    start = current;
    current_region_size = 0;
  }
}
