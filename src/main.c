#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/mman.h>

// array_size < heap region size
#define SMALL_ARRAY_SIZE 500

// array_size > heap region size
#define BIG_ARRAY_SIZE 5000

/**
 * Получает адрес заголовка блока по адресу его содержимого.
 **/
static struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) -
                                 offsetof(struct block_header, contents));
}

typedef void (*test)();

void test_simple_allocate() {
  int *arr = _malloc(SMALL_ARRAY_SIZE * sizeof(int));
  for (size_t i = 0; i < SMALL_ARRAY_SIZE; i++) {
    arr[i] = (int)i;
  }
  for (size_t i = 0; i < SMALL_ARRAY_SIZE; i++) {
    assert(arr[i] == (int)i);
  }
  assert(block_get_header(arr) == HEAP_START);
  debug_heap(stderr, HEAP_START);
  _free(arr);
  debug_heap(stderr, HEAP_START);
}

void test_mmap_extend() {
  int *arr = (int *)_malloc(BIG_ARRAY_SIZE * sizeof(int));
  for (size_t i = 0; i < BIG_ARRAY_SIZE; i++) {
    arr[i] = (int)i;
  }
  for (size_t i = 0; i < BIG_ARRAY_SIZE; i++) {
    assert(arr[i] == (int)i);
  }
  debug_heap(stderr, HEAP_START);
  _free(arr);
  debug_heap(stderr, HEAP_START);
}

void test_mmap_no_extend() {
  void *mmap_addr = mmap(
      HEAP_START + REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_NONE,
      MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED | MAP_FIXED_NOREPLACE, -1, 0);
  assert(mmap_addr != MAP_FAILED);
  debug_heap(stderr, HEAP_START);

  void *first_block = _malloc(500);
  debug_heap(stderr, HEAP_START);
  void *first_block_header = block_get_header(first_block);
  assert(first_block_header == HEAP_START);

  void *second_block = _malloc(REGION_MIN_SIZE);
  debug_heap(stderr, HEAP_START);
  void *second_block_header = block_get_header(second_block);
  assert(second_block_header > HEAP_START + REGION_MIN_SIZE);

  _free(first_block);
  _free(second_block);
}

void test_allocate_2_blocks() {
  void *arr1 = _malloc(BIG_ARRAY_SIZE);
  void *arr2 = _malloc(BIG_ARRAY_SIZE);
  struct block_header *first_block_header = block_get_header(arr1);
  struct block_header *second_block_header = block_get_header(arr2);
  assert(first_block_header == HEAP_START);
  assert(second_block_header ==
         HEAP_START + size_from_capacity(first_block_header->capacity).bytes);

  debug_heap(stderr, HEAP_START);
  _free(arr1);
  _free(arr2);
  debug_heap(stderr, HEAP_START);
  arr1 = _malloc(BIG_ARRAY_SIZE);
  first_block_header = block_get_header(arr1);
  assert(first_block_header == HEAP_START);
  debug_heap(stderr, HEAP_START);
  _free(arr1);
  debug_heap(stderr, HEAP_START);
}

int main() {
  const test tests[] = {
      test_simple_allocate,
      test_allocate_2_blocks,
      test_mmap_extend,
      test_mmap_no_extend,
  };
  const size_t number_of_tests = sizeof(tests) / sizeof(test);

  for (size_t i = 0; i < number_of_tests; i++) {
    fprintf(stdout, "### Test %zu of %zu ###\n", i + 1, number_of_tests);
    heap_init(0);
    tests[i]();
    heap_term();
    fprintf(stdout, "### PASSED ###\n");
  }
}
